// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase:  {
    apiKey: "AIzaSyCDI6eA7ZmbCVADcAlPA3m9sLz3V_IVt14",
    authDomain: "hospitalmanagement-60be2.firebaseapp.com",
    databaseURL: "https://hospitalmanagement-60be2.firebaseio.com",
    projectId: "hospitalmanagement-60be2",
    storageBucket: "hospitalmanagement-60be2.appspot.com",
    messagingSenderId: "323624939036"
  }
};
